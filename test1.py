# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 23:02:56 2015

@author: jp
"""
# imports go here
import numpy as np
from Tkinter import * #Tk, Label, Button

# the transition matrix
A_mat = np.array([[.9, .1], [.1, .9]])
    
# the observation matrix
O_mat = np.array([[.8, .2], [.2, .8]])
    

class transmat:
    def __init__(self,parent):

        A_matdim = self.A_matdim = Toplevel(parent)

        Label(A_matdim, text="What is the dimension of transition matrix?").pack()


        e_Amatdim = self.e_Amatdim = Entry(A_matdim)
        self.e_Amatdim.pack(padx=5)

        b = Button(A_matdim, text="OK", command=self.ok)
        b.pack(pady=5)        
        

    def ok(self):

        print "value is", self.e_Amatdim.get()
        print "\n"

        self.A_matdim.destroy()
        
        
    def matdim(self,e_Amatdim):    
        rows = []
        
        for i in range(e_Amatdim):
            cols = []
            for j in range(self.e_Amatdim):
                e_Amatdim = Entry(relief=RIDGE)
                e_Amatdim.grid(row=i, column=j, sticky=NSEW)
                e_Amatdim.insert(END, '%d.%d' % (i, j))
                cols.append(e_Amatdim)
        rows.append(cols)


        
        
    def onPress():
        for row in rows:
            for col in row:
                print col.get(),
            print  


    Button(text='Fetch', command=onPress).pack()


#root = Tk()
#d = transmat(root)

#root.wait_window(d.top)




class HMMGUI:
    def __init__(self, master):
        self.master = master
        master.title("Hidden Markov Models")
    
        #self.greet_button = Button(master, text='Fetch', command=self.onPress)
        #self.greet_button.pack()
        transmat(master)
              
        self.label = Label(master, text="Click on the algorithm to apply:")
        self.label.pack()

        self.greet_button = Button(master, text="Forward", command=self.fwd_run)
        self.greet_button.pack()
        self.greet_button = Button(master, text="Backward", command=self.bkw_run)
        self.greet_button.pack()
        self.greet_button = Button(master, text="Forward-Backward", command=self.fwd_bkw_run)
        self.greet_button.pack()

        self.close_button = Button(master, text="Close", command=master.quit)
        self.close_button.pack()
        
        print "Transition Matrix:"
        print A_mat
        print "\n"
        print " Observation Matrix:"
        print O_mat
        print "\n"
        
    
       
    def fwd_run(self):
         # the forward matrix
         p,f = fwd(A_mat, O_mat, np.hstack((0,1,1,1)))
         print " Forward Matrix:"
         print p
         print "\n"
         
    def bkw_run(self):
         # the forward matrix
         p,b = bkw(A_mat, O_mat, np.hstack((0,1,1,1)))
         print " Backward Matrix:"
         print p
         print "\n"
         
    def fwd_bkw_run(self):
         # the forward matrix
         p,f,b = fwd_bkw(A_mat, O_mat, np.hstack((0,1,1,1)))
         print " Forward-Backward Matrix:"
         print p
         print "\n"
        
# HMM Algorithms
def fwd(A_mat, O_mat, observ):
    # setup
    k = observ.size
    (n,m) = O_mat.shape
    prob_mat = np.zeros( (n,k) )
    fw = np.zeros( (n,k+1) )
        
    #print "Running Forward Algorithm..."
    # forward part
    fw[:, 0] = 1.0/n
    for obs_ind in xrange(k):
        f_row_vec = np.matrix(fw[:,obs_ind])
        fw[:, obs_ind+1] = f_row_vec * \
                           np.matrix(A_mat) * \
                           np.matrix(np.diag(O_mat[:,observ[obs_ind]]))
        fw[:,obs_ind+1] = fw[:,obs_ind+1]/np.sum(fw[:,obs_ind+1])
    # combine it
    prob_mat = np.array(fw)*1
    prob_mat = prob_mat/np.sum(prob_mat, 0)
    # get out
    return prob_mat, fw
    
def bkw(A_mat, O_mat, observ):
    # setup
    k = observ.size
    (n,m) = O_mat.shape
    prob_mat = np.zeros( (n,k) )
    bw = np.zeros( (n,k+1) )

    # backward part
    bw[:,-1] = 1.0
    for obs_ind in xrange(k, 0, -1):
        b_col_vec = np.matrix(bw[:,obs_ind]).transpose()
        bw[:, obs_ind-1] = (np.matrix(A_mat) * \
                            np.matrix(np.diag(O_mat[:,observ[obs_ind-1]])) * \
                            b_col_vec).transpose()
        bw[:,obs_ind-1] = bw[:,obs_ind-1]/np.sum(bw[:,obs_ind-1])
    # combine it
    prob_mat = 1*np.array(bw)
    prob_mat = prob_mat/np.sum(prob_mat, 0)
    # get out
    return prob_mat, bw

def fwd_bkw(A_mat, O_mat, observ):
    # set up
    k = observ.size
    (n,m) = O_mat.shape
    prob_mat = np.zeros( (n,k) )
    fw = np.zeros( (n,k+1) )
    bw = np.zeros( (n,k+1) )
    # forward part
    fw[:, 0] = 1.0/n
    for obs_ind in xrange(k):
        f_row_vec = np.matrix(fw[:,obs_ind])
        fw[:, obs_ind+1] = f_row_vec * \
                           np.matrix(A_mat) * \
                           np.matrix(np.diag(O_mat[:,observ[obs_ind]]))
        fw[:,obs_ind+1] = fw[:,obs_ind+1]/np.sum(fw[:,obs_ind+1])
    # backward part
    bw[:,-1] = 1.0
    for obs_ind in xrange(k, 0, -1):
        b_col_vec = np.matrix(bw[:,obs_ind]).transpose()
        bw[:, obs_ind-1] = (np.matrix(A_mat) * \
                        np.matrix(np.diag(O_mat[:,observ[obs_ind-1]])) * \
                        b_col_vec).transpose()
        bw[:,obs_ind-1] = bw[:,obs_ind-1]/np.sum(bw[:,obs_ind-1])
    # combine it
    prob_mat = np.array(fw)*np.array(bw)
    prob_mat = prob_mat/np.sum(prob_mat, 0)
    # get out
    return prob_mat, fw, bw
    

root = Tk()
my_gui = HMMGUI(root)
root.mainloop()