# -*- coding: utf-8 -*-
"""
Created on Sun Apr 12 02:53:20 2015

@author: jp
"""
from Tkinter import *

class Application(Frame) :
    
    def __init__(self,master):
        Frame.__init__(self,master)
        self.grid()
        self.create_widgets()
            
    def create_widgets(self):
        self.label1 = Label(self, text="Choose the Hidden Markov Method ")
        self.label1.grid()
        self.label2 = Label(self, text=" Algorithm that you want to execute: ")
        self.label2.grid()
        
        ALGORITHMS = [("Forward", "fwd"), 
                      ("Forward Backward", "fwd_bkw"), 
                      ("Viterbi", "viterbi")]
                      
        v = StringVar()
        v.set("") # initialize
                      
        for text, mode in ALGORITHMS:
            self.radio1 = Radiobutton(self,text=text,variable=v,value=mode)
            self.radio1.config(justify=LEFT,anchor=W)
            self.radio1.grid()
            
        self.button1 = Button(self, text="Execute")
        self.button1.grid()
        self.button2 = Button(self, text="Exit", command=exit)
        self.button2.grid()
        
root = Tk()
root.title("HMM Algorithms")
root.geometry("275x200")

app = Application(root)

root.mainloop()

