# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 16:32:32 2015

@author: jopires
"""

# imports go here
import numpy as np

# HMM Algorithms
def fwd(A_mat, O_mat, observ):
    # set up
    k = observ.size
    (n,m) = O_mat.shape
    prob_mat = np.zeros( (n,k) )
    fw = np.zeros( (n,k+1) )
    
    # forward part
    fw[:, 0] = 1.0/n
    for obs_ind in xrange(k):
        f_row_vec = np.matrix(fw[:,obs_ind])
        fw[:, obs_ind+1] = f_row_vec * \
                           np.matrix(A_mat) * \
                           np.matrix(np.diag(O_mat[:,observ[obs_ind]]))
        fw[:,obs_ind+1] = fw[:,obs_ind+1]/np.sum(fw[:,obs_ind+1])
     # combine it
    prob_mat = np.array(fw)*1
    prob_mat = prob_mat/np.sum(prob_mat, 0)
    # get out
    return prob_mat, fw
    
def bkw(A_mat, O_mat, observ):
    # set up
    k = observ.size
    (n,m) = O_mat.shape
    prob_mat = np.zeros( (n,k) )
    bw = np.zeros( (n,k+1) )

    # backward part
    bw[:,-1] = 1.0
    for obs_ind in xrange(k, 0, -1):
        b_col_vec = np.matrix(bw[:,obs_ind]).transpose()
        bw[:, obs_ind-1] = (np.matrix(A_mat) * \
                            np.matrix(np.diag(O_mat[:,observ[obs_ind-1]])) * \
                            b_col_vec).transpose()
        bw[:,obs_ind-1] = bw[:,obs_ind-1]/np.sum(bw[:,obs_ind-1])
    # combine it
    prob_mat = 1*np.array(bw)
    prob_mat = prob_mat/np.sum(prob_mat, 0)
    # get out
    return prob_mat, bw

def fwd_bkw(A_mat, O_mat, observ):
    # set up
    k = observ.size
    (n,m) = O_mat.shape
    prob_mat = np.zeros( (n,k) )
    fw = np.zeros( (n,k+1) )
    bw = np.zeros( (n,k+1) )
    # forward part
    fw[:, 0] = 1.0/n
    for obs_ind in xrange(k):
        f_row_vec = np.matrix(fw[:,obs_ind])
        fw[:, obs_ind+1] = f_row_vec * \
                           np.matrix(A_mat) * \
                           np.matrix(np.diag(O_mat[:,observ[obs_ind]]))
        fw[:,obs_ind+1] = fw[:,obs_ind+1]/np.sum(fw[:,obs_ind+1])
    # backward part
    bw[:,-1] = 1.0
    for obs_ind in xrange(k, 0, -1):
        b_col_vec = np.matrix(bw[:,obs_ind]).transpose()
        bw[:, obs_ind-1] = (np.matrix(A_mat) * \
                            np.matrix(np.diag(O_mat[:,observ[obs_ind-1]])) * \
                            b_col_vec).transpose()
        bw[:,obs_ind-1] = bw[:,obs_ind-1]/np.sum(bw[:,obs_ind-1])
    # combine it
    prob_mat = np.array(fw)*np.array(bw)
    prob_mat = prob_mat/np.sum(prob_mat, 0)
    # get out
    return prob_mat, fw, bw
    
    



if __name__ == "__main__":
  

    # the transition matrix
    A_mat = np.array([[.9, .1], [.1, .9]])
    print " Transition Matrix:"
    print A_mat
    print "\n"

    # the observation matrix
    O_mat = np.array([[.8, .2], [.2, .8]])
    print " Observation Matrix:"
    print O_mat
    print "\n"

    # the forward matrix
    p,f = fwd(A_mat, O_mat, np.hstack((0,1,1,1)))
    print " Forward Matrix:"
    print p
    print "\n"
            
    # the backward matrix
    p,b = bkw(A_mat, O_mat, np.hstack((0,1,1,1)))
    print " Backward Matrix:"
    print p
    print "\n"
    
     # the forward-backward matrix
    p,f,b = fwd_bkw(A_mat, O_mat, np.hstack((0,1,1,1)))
    print " Forward-Backward Matrix:"
    print p
    print "\n"
