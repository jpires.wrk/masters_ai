# -*- coding: utf-8 -*-
"""
Spyder Editor
Solution for Exercise 6 - SCAD - Actividade 6
m34004@alunos.uevora.pt
created at: 2015/05/29

"""

import mdptoolbox
import numpy as np
P = np.array([[[0.9, 0.1],[0.1, 0.9]],[[0.1, 0.9],[0.9, 0.1]]])
print "Probability Matrix:"
print P
R1 = np.array([[1, 2], [2, 1]])
print "***************************"
print "* Reward Matrix with C=0: *"
print "***************************"
print R1
rvi1 = mdptoolbox.mdp.RelativeValueIteration(P, R1)
rvi1.run()
print "Values:"
print rvi1
print"Average Reward:"
print rvi1.average_reward
print "Policy:"
print rvi1.policy
print "Iter:"
print rvi1.iter
R2 = np.array([[2, 0.5], [2,-0.5]])
print "*****************************"
print "* Reward Matrix with C=1.5: *"
print "*****************************"
print R2
rvi2 = mdptoolbox.mdp.RelativeValueIteration(P, R2)
rvi2.run()
print "Values:"
print rvi2
print"Average Reward:"
print rvi2.average_reward
print "Policy:"
print rvi2.policy
print "Iter:"
print rvi2.iter




