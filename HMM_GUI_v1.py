# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 23:02:56 2015

@author: jp
"""
# imports go here
import numpy as np
from Tkinter import * #Tk, Label, Button

# the transition matrix
A_mat = np.array([[.9, .1], [.1, .9]])
    
# the observation matrix
O_mat = np.array([[.8, .2], [.2, .8]])
    

class MyDialog:
    def __init__(self, parent):

        top = self.top = Toplevel(parent)

        Label(top, text="Value").pack()

        self.e = Entry(top)
        self.e.pack(padx=5)

        b = Button(top, text="OK", command=self.ok)
        b.pack(pady=5)

    def ok(self):

        print "value is", self.e.get()

        self.top.destroy()


#root = Tk()
#d = MyDialog(root)

#root.wait_window(d.top)




class HMMGUI:
    def __init__(self, master):
        self.master = master
        master.title("Hidden Markov Models")
    
        #self.greet_button = Button(master, text='Fetch', command=self.onPress)
        #self.greet_button.pack()
        
              
        self.label = Label(master, text="Click on the algorithm to apply:")
        self.label.pack()

        self.greet_button = Button(master, text="Forward", command=self.fwd_run)
        self.greet_button.pack()
        self.greet_button = Button(master, text="Backward", command=self.bkw_run)
        self.greet_button.pack()
        self.greet_button = Button(master, text="Forward-Backward", command=self.fwd_bkw_run)
        self.greet_button.pack()
        self.greet_button = Button(master, text="Viterbi", command=self.viterbi_run)
        self.greet_button.pack()
        
        self.close_button = Button(master, text="Close", command=master.quit)
        self.close_button.pack()
        
        print "Transition Matrix:"
        print A_mat
        print "\n"
        print " Observation Matrix:"
        print O_mat
        print "\n"
        
    
       
    def fwd_run(self):
         # the forward matrix
         p,f = fwd(A_mat, O_mat, np.hstack((0,1,1,1)))
         print " Forward Matrix:"
         print p
         print "\n"
         
    def bkw_run(self):
         # the forward matrix
         p,b = bkw(A_mat, O_mat, np.hstack((0,1,1,1)))
         print " Backward Matrix:"
         print p
         print "\n"
         
    def fwd_bkw_run(self):
         # the forward matrix
         p,f,b = fwd_bkw(A_mat, O_mat, np.hstack((0,1,1,1)))
         print " Forward-Backward Matrix:"
         print p
         print "\n"
         
    def viterbi_run(self):
        # we have what we need, do viterbi
        obs = np.hstack((0,1,1,1))
        best_path_ind, paths, log_probs = viterbi(A_mat, O_mat, obs)
        print "Viterbi results:\n"        
        print "obs is " + str(obs) + "\n"
        print "obs, best path is" + str(paths[best_path_ind,:]) + "\n"
    # change observations to reflect messed up ratio
    #observations2 = np.random.random(num_obs)
    #observations2[observations2>.85] = 0
    #observations2[observations2<=.85] = 1
    # majority of the time its tails, now what?
    #best_path_ind, paths, log_probs = viterbi_alg(A_mat, O_mat, observations2)
    #print "obs2 is " + str(observations1)
    #print "obs2, best path is" + str(paths[best_path_ind,:])
    #have it switch partway?
    #best_path_ind, paths, log_probs = viterbi_alg(A_mat, \
    #                                      O_mat, \
    #                                      np.hstack( (observations1, observations2) ) )
    #print "obs12 is " + str(np.hstack( (observations1, observations2) ) )
    #print "obs12, best path is" + str(paths[best_path_ind,:])

         # the Viterbi matrix
    #     p,f,b = viterbi(A_mat, O_mat, np.hstack((0,1,1,1)))
        print " Viterbi Matrix:"
        print log_probs
        #print best_path_ind, paths, log_probs
        print "\n"
        
# HMM Algorithms
def fwd(A_mat, O_mat, observ):
    # setup
    k = observ.size
    (n,m) = O_mat.shape
    prob_mat = np.zeros( (n,k) )
    fw = np.zeros( (n,k+1) )
        
    #print "Running Forward Algorithm..."
    # forward part
    fw[:, 0] = 1.0/n
    for obs_ind in xrange(k):
        f_row_vec = np.matrix(fw[:,obs_ind])
        fw[:, obs_ind+1] = f_row_vec * \
                           np.matrix(A_mat) * \
                           np.matrix(np.diag(O_mat[:,observ[obs_ind]]))
        fw[:,obs_ind+1] = fw[:,obs_ind+1]/np.sum(fw[:,obs_ind+1])
    # combine it
    prob_mat = np.array(fw)*1
    prob_mat = prob_mat/np.sum(prob_mat, 0)
    # get out
    return prob_mat, fw
    
def bkw(A_mat, O_mat, observ):
    # setup
    k = observ.size
    (n,m) = O_mat.shape
    prob_mat = np.zeros( (n,k) )
    bw = np.zeros( (n,k+1) )

    # backward part
    bw[:,-1] = 1.0
    for obs_ind in xrange(k, 0, -1):
        b_col_vec = np.matrix(bw[:,obs_ind]).transpose()
        bw[:, obs_ind-1] = (np.matrix(A_mat) * \
                            np.matrix(np.diag(O_mat[:,observ[obs_ind-1]])) * \
                            b_col_vec).transpose()
        bw[:,obs_ind-1] = bw[:,obs_ind-1]/np.sum(bw[:,obs_ind-1])
    # combine it
    prob_mat = 1*np.array(bw)
    prob_mat = prob_mat/np.sum(prob_mat, 0)
    # get out
    return prob_mat, bw

def fwd_bkw(A_mat, O_mat, observ):
    # set up
    k = observ.size
    (n,m) = O_mat.shape
    prob_mat = np.zeros( (n,k) )
    fw = np.zeros( (n,k+1) )
    bw = np.zeros( (n,k+1) )
    # forward part
    fw[:, 0] = 1.0/n
    for obs_ind in xrange(k):
        f_row_vec = np.matrix(fw[:,obs_ind])
        fw[:, obs_ind+1] = f_row_vec * \
                           np.matrix(A_mat) * \
                           np.matrix(np.diag(O_mat[:,observ[obs_ind]]))
        fw[:,obs_ind+1] = fw[:,obs_ind+1]/np.sum(fw[:,obs_ind+1])
    # backward part
    bw[:,-1] = 1.0
    for obs_ind in xrange(k, 0, -1):
        b_col_vec = np.matrix(bw[:,obs_ind]).transpose()
        bw[:, obs_ind-1] = (np.matrix(A_mat) * \
                        np.matrix(np.diag(O_mat[:,observ[obs_ind-1]])) * \
                        b_col_vec).transpose()
        bw[:,obs_ind-1] = bw[:,obs_ind-1]/np.sum(bw[:,obs_ind-1])
    # combine it
    prob_mat = np.array(fw)*np.array(bw)
    prob_mat = prob_mat/np.sum(prob_mat, 0)
    # get out
    return prob_mat, fw, bw
    
def viterbi(A_mat, O_mat, observations):
    # get number of states
    num_obs = observations.size
    num_states = A_mat.shape[0]
    # initialize path costs going into each state, start with 0
    log_probs = np.zeros(num_states)
    # initialize arrays to store best paths, 1 row for each ending state
    paths = np.zeros( (num_states, num_obs+1 ))
    paths[:, 0] = np.arange(num_states)
    # start looping
    for obs_ind, obs_val in enumerate(observations):
        # for each obs, need to check for best path into each state
        for state_ind in xrange(num_states):
            # given observation, check prob of each path
            temp_probs = log_probs + \
                         np.log(O_mat[state_ind, obs_val]) + \
                         np.log(A_mat[:, state_ind])
            # check for largest score
            best_temp_ind = np.argmax(temp_probs)
            # save the path with a higher prob and score
            paths[state_ind,:] = paths[best_temp_ind,:]
            paths[state_ind,(obs_ind+1)] = state_ind
            log_probs[state_ind] = temp_probs[best_temp_ind]
    # we now have a best stuff going into each path, find the best score
    best_path_ind = np.argmax(log_probs)
    # done, get out.
    return (best_path_ind, paths, log_probs)

root = Tk()
my_gui = HMMGUI(root)
root.mainloop()