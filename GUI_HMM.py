# -*- coding: utf-8 -*-
"""
Created on Sun Apr 12 02:35:07 2015

@author: jp
"""

# simple GUI
from Tkinter import *

# create de window
root = Tk()

# modify root window
root.title("HMM Algorithms")
root.geometry("275x200")

app = Frame(root)
app.grid()
label1 = Label(app, text="Choose the Hidden Markov Method ")
label2 = Label(app, text=" Algorithm that you want to execute: ")
button1 = Button(app, text="Execute")
button2 = Button(app, text="Exit")

label1.grid()
label2.grid()
button1.grid()
button2.grid()
# kick off the event loop
root.mainloop()

