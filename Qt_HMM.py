# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'untitled.ui'
#
# Created: Sun Apr 12 22:30:53 2015
#      by: PyQt4 UI code generator 4.9.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(394, 206)
        self.buttonBox = QtGui.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(290, 20, 81, 241))
        self.buttonBox.setOrientation(QtCore.Qt.Vertical)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.groupBox = QtGui.QGroupBox(Dialog)
        self.groupBox.setGeometry(QtCore.QRect(20, 20, 241, 211))
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.fwd_rd = QtGui.QRadioButton(Dialog)
        self.fwd_rd.setGeometry(QtCore.QRect(30, 50, 98, 20))
        self.fwd_rd.setObjectName(_fromUtf8("fwd_rd"))
        self.baum_welch_rb = QtGui.QRadioButton(Dialog)
        self.baum_welch_rb.setGeometry(QtCore.QRect(30, 170, 98, 20))
        self.baum_welch_rb.setObjectName(_fromUtf8("baum_welch_rb"))
        self.bkw_rd = QtGui.QRadioButton(Dialog)
        self.bkw_rd.setGeometry(QtCore.QRect(30, 80, 98, 20))
        self.bkw_rd.setObjectName(_fromUtf8("bkw_rd"))
        self.fwd_bkw_rd = QtGui.QRadioButton(Dialog)
        self.fwd_bkw_rd.setGeometry(QtCore.QRect(30, 110, 141, 20))
        self.fwd_bkw_rd.setObjectName(_fromUtf8("fwd_bkw_rd"))
        self.viterbi_rd = QtGui.QRadioButton(Dialog)
        self.viterbi_rd.setGeometry(QtCore.QRect(30, 140, 98, 20))
        self.viterbi_rd.setObjectName(_fromUtf8("viterbi_rd"))

        self.retranslateUi(Dialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), Dialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "Hidden Markov Method", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox.setTitle(QtGui.QApplication.translate("Dialog", "HMM Algorithms", None, QtGui.QApplication.UnicodeUTF8))
        self.fwd_rd.setText(QtGui.QApplication.translate("Dialog", "Forward", None, QtGui.QApplication.UnicodeUTF8))
#        self.baum_welch_rd.setText(QtGui.QApplication.translate("Dialog", "Baum Welch", None, QtGui.QApplication.UnicodeUTF8))
        self.bkw_rd.setText(QtGui.QApplication.translate("Dialog", "Backward", None, QtGui.QApplication.UnicodeUTF8))
        self.fwd_bkw_rd.setText(QtGui.QApplication.translate("Dialog", "Forward Backward", None, QtGui.QApplication.UnicodeUTF8))
#        self.viterbi_rd.setText(QtGui.QApplication.translate("Dialog", "Viterbi", None, QtGui.QApplication.UnicodeUTF8))


# HMM Algorithms
def fwd(A_mat, O_mat, observ):
    # set up
    k = observ.size
    (n,m) = O_mat.shape
    prob_mat = np.zeros( (n,k) )
    fw = np.zeros( (n,k+1) )
    
    # forward part
    fw[:, 0] = 1.0/n
    for obs_ind in xrange(k):
        f_row_vec = np.matrix(fw[:,obs_ind])
        fw[:, obs_ind+1] = f_row_vec * \
                           np.matrix(A_mat) * \
                           np.matrix(np.diag(O_mat[:,observ[obs_ind]]))
        fw[:,obs_ind+1] = fw[:,obs_ind+1]/np.sum(fw[:,obs_ind+1])
     # combine it
    prob_mat = np.array(fw)*1
    prob_mat = prob_mat/np.sum(prob_mat, 0)
    # get out
    return prob_mat, fw
    
def bkw(A_mat, O_mat, observ):
    # set up
    k = observ.size
    (n,m) = O_mat.shape
    prob_mat = np.zeros( (n,k) )
    bw = np.zeros( (n,k+1) )

    # backward part
    bw[:,-1] = 1.0
    for obs_ind in xrange(k, 0, -1):
        b_col_vec = np.matrix(bw[:,obs_ind]).transpose()
        bw[:, obs_ind-1] = (np.matrix(A_mat) * \
                            np.matrix(np.diag(O_mat[:,observ[obs_ind-1]])) * \
                            b_col_vec).transpose()
        bw[:,obs_ind-1] = bw[:,obs_ind-1]/np.sum(bw[:,obs_ind-1])
    # combine it
    prob_mat = 1*np.array(bw)
    prob_mat = prob_mat/np.sum(prob_mat, 0)
    # get out
    return prob_mat, bw

def fwd_bkw(A_mat, O_mat, observ):
    # set up
    k = observ.size
    (n,m) = O_mat.shape
    prob_mat = np.zeros( (n,k) )
    fw = np.zeros( (n,k+1) )
    bw = np.zeros( (n,k+1) )
    # forward part
    fw[:, 0] = 1.0/n
    for obs_ind in xrange(k):
        f_row_vec = np.matrix(fw[:,obs_ind])
        fw[:, obs_ind+1] = f_row_vec * \
                           np.matrix(A_mat) * \
                           np.matrix(np.diag(O_mat[:,observ[obs_ind]]))
        fw[:,obs_ind+1] = fw[:,obs_ind+1]/np.sum(fw[:,obs_ind+1])
    # backward part
    bw[:,-1] = 1.0
    for obs_ind in xrange(k, 0, -1):
        b_col_vec = np.matrix(bw[:,obs_ind]).transpose()
        bw[:, obs_ind-1] = (np.matrix(A_mat) * \
                            np.matrix(np.diag(O_mat[:,observ[obs_ind-1]])) * \
                            b_col_vec).transpose()
        bw[:,obs_ind-1] = bw[:,obs_ind-1]/np.sum(bw[:,obs_ind-1])
    # combine it
    prob_mat = np.array(fw)*np.array(bw)
    prob_mat = prob_mat/np.sum(prob_mat, 0)
    # get out
    return prob_mat, fw, bw
    
    

if __name__ == "__main__":
    import sys
    app = QtGui.QApplication(sys.argv)
    Dialog = QtGui.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

    print option.get()
    if option.get() == "fwd_rd":
        button1.configure(command=fwd)
    elif option.get() == "bkw_rd":
        button1.configure(command=bkd)
    elif option.get() == "fwd_bkw_rd":
        button1.configure(command=fwd_bkd)
    elif option.get() == "viterbi_rd":
        button1.configure(command=viterbi)
    elif option.get() == "baum_welch_rd":
        button1.configure(command=baum_welch)
    else:
        print "Nothing to do!"